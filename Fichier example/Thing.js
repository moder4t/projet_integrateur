// components/iotobject.js

import React from 'react';
import { StyleSheet, View, Text, Switch, TouchableOpacity, Button} from 'react-native';
import { Client, Message } from 'react-native-paho-mqtt';
import Slider from '@react-native-community/slider';
//import test from '../conponents/t'
import GLOBALS from './Global'
import Events from './events';
import { __SECRET_INTERNALS_DO_NOT_USE_OR_YOU_WILL_BE_FIRED } from 'react-native/Libraries/Renderer/implementations/ReactNativeRenderer-prod';


// const onRemove = id => e => {
//     __SECRET_INTERNALS_DO_NOT_USE_OR_YOU_WILL_BE_FIRED(todos.filter(todo => todo.id !== id));
// };
// const TodoList = ({todos, onRemove}) => {
//     return (
//       <ScrollView contentContainerStyle={styles.listContainer}>
//         {todos.map(todo => (
//           <TodoListItem key={todo.id} {...todo} onRemove={onRemove} />
//         ))}
//       </ScrollView>
//     );
//   };

class Thing extends React.Component {

    state = {value: true};

    // TODO: passer le ip et le mac selon l'object (passer l'information d'une view a l'autre)
    ip = '123'
    mac= '132'

    async componentDidMount() {
        this.setState({value: this.props.value});
        // Check if the event log is empty
        const events = new Events();
        this.state.empty = await events.getEventLogs().lenght == 0 ? true : false;

        // Perform MQTT initialisation
        this.client = new Client({ uri: 'ws://test.mosquitto.org:8080/', clientId: 'test' });
        this.client.connect()
        .then(() => {
            console.log('onConnect');
            return this.client.subscribe(`/IOT3/STATES/${ip}`); 
            })
                .then(() => {
                console.log('onSubcribe');
            });

            // IoT state has changed {"id": "SmartPlugStub", "cmd": "off"}
            this.client.on('messageReceived', async (message) => {
                console.log(message.payloadString);
            })
    }
    setBlindToManualOpen(caption, value) {
        // 
        const message = new Message(JSON.stringify({ id: "", cmd: GLOBALS.MQTT_CMD_OPEN_BLINDS }));
        message.destinationName = `/IOT3/COMMANDS/${ip}/${mac}`;
        this.client.send(message);
    }
    setBlindToManualClose(caption, value) {    
        // 
        const message = new Message(JSON.stringify({ id: "", cmd: GLOBALS.MQTT_CMD_CLOSE_BLINDS }));
        message.destinationName = `/IOT3/COMMANDS/${ip}/${mac}`;
        this.client.send(message);
    }
    setBlindToAuto(caption, value) {    
        // 
        const message = new Message(JSON.stringify({ id: "", cmd: GLOBALS.MQTT_CMD_AUTO_BLIND }));
        message.destinationName = `/IOT3/COMMANDS/${ip}/${mac}`;
        this.client.send(message);
    }
    setBlindToSmart(caption, value) {    
        // 
        const message = new Message(JSON.stringify({ id: "", cmd: GLOBALS.MQTT_CMD_SMART_BLIND }));
        message.destinationName = `/IOT3/COMMANDS/${ip}/${mac}`;
        this.client.send(message);
    }

    render() {
        return (
            <View style={styles.container}>
                <View style={styles.infos}>
                    <Text style={styles.name}>{this.props.caption}</Text>
                    <Text style={styles.value}>Etat du store: presentement {this.props.value ? 'OUVERT': 'FERMER'}</Text>
                </View>
                <View style={styles.switch}>
                    <Text style={styles.innerText}>Mode Automatique</Text>
                    <Switch value={this.props.value} 
                        onValueChange={(value)=>{
                            this.props.setBlindToAuto(this.props.caption, value);
                        }}>
                    </Switch>
                </View>
                <View style={styles.manuel}>
                    <Text style={styles.innerText}>Mode manuel</Text>
                    <Switch value={this.props.value} 
                        onValueChange={(value)=>{
                            this.props.onValueChange(this.props.caption, value);
                        }}>
                    </Switch>
                    <Button
                        title="Ouvrir"
                        onPress={() => setBlindToManualOpen('')}
                    />
                    <Button
                        title="Fermer"
                        onPress={() => setBlindToManualClose('')}
                    />
                </View>
                <View style={styles.switch}>
                    <Text style={styles.innerText}>Mode intelligent</Text>

                    <Switch value={this.props.value} 
                        onValueChange={(value)=>{
                            this.props.setBlindToSmart(this.props.caption, value);
                        }}>
                    </Switch>
                    
                </View>
                <TouchableOpacity
                    style={styles.bottomView}
                    onPress={() => this.props.navigation.deleteItem()}
                    >                
                <Text style={styles.footer}>Suprimer ce store</Text>
                </TouchableOpacity>
            </View>
        )
    }
}



const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#232426',
        },
    infos: {
        alignItems: 'center',
        justifyContent: 'center',
        flex: 1,
    },


    switch: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
        marginTop: 5,
        marginLeft: 5,
        marginRight: 5,
        marginBottom: 5,
        padding:10,
    },

    manuel:{
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
        marginTop: 5,
        marginLeft: 5,
        marginRight: 5,
        marginBottom: 5,
        padding:10,
    },

    bottomView:{
 
        width: '100%', 
        height: 50, 
        backgroundColor: '#04BF68', 
        justifyContent: 'center', 
        alignItems: 'center',
        flexDirection:'row',
        borderRadius:10,
        marginBottom:5,
      },  

    innerText:{
        color: "white",
    },
    value:{
        color: "white",
    },
    footer:{
        fontWeight: 'bold'
    }
});

export default Thing