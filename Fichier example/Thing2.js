// components/iotobject.js

import React from 'react'
import { StyleSheet, View, Text, Switch } from 'react-native'


class Thing extends React.Component {

    state = {value: true};
    
    componentDidMount() {
        this.setState({value: this.props.value});
    }

    render() {
        return (
            <View style={styles.container}>
                <View style={styles.infos}>
                    <Text style={styles.name}>{this.props.caption}</Text>
                    <Text style={styles.value}>{this.props.value ? 'ON': 'OFF'}</Text>
                </View>
                <View style={styles.switch}>
                    <Switch value={this.props.value} 
                        onValueChange={(value)=>{
                            this.props.onValueChange(this.props.caption, value);
                        }}>
                    </Switch>
                </View>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flexDirection: "row",
        width: "auto",
        borderColor: "blue",
        borderWidth: 1,
        borderRadius: 5,
        marginTop: 5,
        marginLeft: 5,
        marginRight: 5,
        marginBottom: 5
    },
    infos: {
        flex: 3,
        alignItems: 'flex-start',
        justifyContent: 'center',
        marginTop: 5,
        marginLeft: 5,
        marginRight: 5,
        marginBottom: 5
    },
    switch: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
        marginTop: 5,
        marginLeft: 5,
        marginRight: 5,
        marginBottom: 5
    }
});

export default Thing