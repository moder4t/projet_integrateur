// components/AffHistory.js class History

import React from 'react'
import { StyleSheet, SafeAreaView, View, Text, FlatList, Button, StatusBar } from 'react-native'
import Events from './events';


class History extends React.Component {

    // Ajout de la couleur https://reactnavigation.org/docs/4.x/stack-navigator#navigationoptions-used-by-stacknavigator
    static navigationOptions = {
        headerTitle: "Historique des commandes",
        headerRight: () => (
            <Button
                onPress={ () => {
                  alert('Merci pour votre cours!');
                }}
                title="Secret"
            />
        ),
    };
    state = {
      bd: [],
  }

  componentDidMount() {
  }
 
    render() {
        const { navigation } = this.props;
        const BD = navigation.getParam('bd')

        const Item = ({ title, datetime }) => (
          <View style={styles.title}>
            <Text style={styles.texto}>{title}</Text>
            <Text style={styles.texto}>When: {datetime}</Text>
          </View>
        );

        const renderItem = ({ item }) => (
            <Item title={item.title} datetime={item.when}/>
          );  

        return (
            <SafeAreaView style={styles.container}>
              <FlatList
                data={BD}
                renderItem={renderItem}
                keyExtractor={item => item.id}
              />
              <Button 
                onPress={ async () => {

                  // clear all event logs
                  const events = new Events();
                  await events.clear();

                  // navigate to the home screen
                  this.props.navigation.navigate('Home');

                }} 
                title='Clear'
            />
            </SafeAreaView>
          );
    }
}

const styles = StyleSheet.create({
    wrapper: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center'
    },
    container: {
        flex: 1,
        marginTop: StatusBar.currentHeight || 0,
    },
    title: {
        borderWidth: 2,
        fontSize : 20,
        backgroundColor: 'lightblue',
        padding: 20,
        marginVertical: 8,
        marginHorizontal: 16,
    },
    texto:{
        fontSize: 20,
        fontWeight: 'bold',
    }
});

export default History

