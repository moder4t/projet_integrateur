// components/Navigation.js

import { createAppContainer } from 'react-navigation'
import { createStackNavigator } from 'react-navigation-stack'
import Home from '../conponents/Home'
import History from '../conponents/AffHistory'


const Navigation = createStackNavigator(
  {
    Home: Home,
    History: History
  },
  {
    initialRouteName: 'Home',
  }
) 

export default createAppContainer(Navigation)
