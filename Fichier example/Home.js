// components/Home.js

import React from 'react'
import { StyleSheet, View, Text, Button } from 'react-native'
import Thing from './Thing';
import { Client, Message } from 'react-native-paho-mqtt';
import Events from './events';


const myStorage = {
    setItem: (key, item) => {
        myStorage[key] = item;
    },
    getItem: (key) => myStorage[key],
    removeItem: (key) => {
        delete myStorage[key];
    },
};


class Home extends React.Component {

    static navigationOptions = {
        title: 'Ma maison'
    };

    state = {
        entrance: false,
        living: false,
        alarm: false/*,
        empty: true*/
    };

    async componentDidMount() {

        // Check if the event log is empty
        const events = new Events();
        this.state.empty = await events.getEventLogs().lenght == 0 ? true : false;

        // Perform MQTT initialisation
        this.client = new Client({ uri: 'ws://test.mosquitto.org:8080/', clientId: 'Allo', storage: myStorage });
        this.client.connect()
        .then(() => {
            console.log('onConnect');
            return this.client.subscribe('Gills/Etats'); 
            })
                .then(() => {
                console.log('onSubcribe');
            });

            // IoT state has changed {"id": "SmartPlugStub", "cmd": "off"}
            this.client.on('messageReceived', async (message) => {
                console.log(message.payloadString);

                // Update user interface
                const msg = JSON.parse(message.payloadString);
                if (msg["id"] == "SmartPlugStub") {


                    // log the new event
                    const events = new Events();
                    await events.logEvent(new Date(), msg["id"], msg["cmd"]);

                    // update the view
                    this.setState({
                        entrance: msg["cmd"] == "on" ? true : false,
                        living: this.state.living,
                        alarm: this.state.alarm/*,
                        empty: this.state.empty*/
                    });
                }
                else if (msg["id"] == "SysAlarmStub") {

                    // log the new event
                    const events = new Events();
                    await events.logEvent(new Date(), msg["id"], msg["cmd"]);

                    // update the view
                    this.setState({
                        entrance: this.state.entrance,
                        living: this.state.living,
                        alarm: msg["cmd"] == "on" ? true : false/*,
                        empty: this.state.empty*/
                    });
                }

        });
    }

    onEntranceChange(caption, value) {

        // TODO: send MQTT command to update the state of the IoT object 1
        const message = new Message(JSON.stringify({ id: "SmartPlugProxy", cmd: value ? "on" : "off" }));
        message.destinationName = 'Gills/Commandes';
        this.client.send(message);

    }

    onLivingChange(caption, value) {
 
        // TODO: send MQTT command to update the state of the IoT object 2
        const message = new Message(JSON.stringify({ id: "SmartPlugProxy", cmd: value ? "on" : "off" }));
        message.destinationName = 'Gills/Commandes';
        this.client.send(message);
    }

    onAlarmChange(caption, value) {

        // TODO: send MQTT command to update the state of the IoT object 3
        const message = new Message(JSON.stringify({ id: "AlarmProxy", cmd: value ? "on" : "off" }));
        message.destinationName = 'Gills/Commandes';
        this.client.send(message);
    }

    render() {
        return (
            <View style={styles.wrapper}>
                <View style={{flex: 0.9}}>
                    <Thing caption={'Lumière entrée'} value={this.state.entrance} onValueChange={(caption, value) => {
                        this.onEntranceChange(caption, value);
                    }} />
                    <Thing caption={'Lumière salon'} value={this.state.living} onValueChange={(caption, value) => {
                        this.onLivingChange(caption, value);
                    }} />
                    <Thing caption={'Alarme'} value={this.state.alarm} onValueChange={(caption, value) => {
                        this.onAlarmChange(caption, value);
                    }} />
                </View>
                <View style={{flex: 0.1}}>                  
                    <Button title="Voir l'Historique des commandes" 

                        /*disabled={ async () => {
                            const events = new Events();
                            return await events.getEventLogs().lenght == 0 ? true : false;
                        }}*/

                        /*disabled={this.state.empty}*/
            
                        onPress={
                            async () => {

                                // Init. local storage
                                const events = new Events(); 
                                const DATA = await events.getEventLogs(); 
                                this.props.navigation.navigate('History', {bd:DATA});
                            }
                        }
                    />
                </View>
            </View>
        )
        
    }
}

const styles = StyleSheet.create({
    wrapper: {
        flex: 1
    },
    history: {
        alignItems: 'center',
        justifyContent: 'center'
    }
});

export default Home
