// components/Navigation.js

import { createAppContainer } from 'react-navigation'
import { createStackNavigator } from 'react-navigation-stack'
import Home from '../conponents/Home'
import Ajout from '../conponents/Ajout'
import Thing from '../conponents/Thing'


const Navigation = createStackNavigator(
  {
    Home: Home,
    History: History,
    Ajout: Ajout,
    Thing: Thing,
  },
  {
    initialRouteName: 'Home',
  }
) 

export default createAppContainer(Navigation)
