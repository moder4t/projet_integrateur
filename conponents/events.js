import AsyncStorage from '@react-native-async-storage/async-storage';


class Events {

    constructor(){
        this.key = 'key';
    }

    async logEvent(date, object, state) {
        console.log('logEvent');
        try {
            // list all event logs
            const logs = await this.getEventLogs();

            // get the greatest id in the event logs
            let id = 0;
            for (let i = 0; i < logs.length; i++) {
                if (logs[i].id > id) {
                    id = logs[i].id;
                }
            }

            // create a unique identifier
            id++;
            
            // construct a new event log and add it to the list
            var today = new Date();
            var date = today.getFullYear() + '-' + (today.getMonth() + 1) + '-' + today.getDate();
            var time = today.getHours() + ':' + today.getMinutes() + ':' + today.getSeconds();
            // var formatted = new Intl.DateTimeFormat("en-US", { dateStyle: 'full', timeStyle: 'long' }).format(date);
            const event = {id: id, when:  date + ' ' + time, title: object + ' ' + state};
            logs.unshift(event);

            // save the list
            await AsyncStorage.setItem(this.key, JSON.stringify({ logs: logs }));
        }
        catch (error) {
            console.log(error);
        }
    }

    async getEventLogs(){
        try {
            let events = await AsyncStorage.getItem(this.key);
            if (events == null) {
                return [];
            }
            return JSON.parse(events).logs;
        }
        catch (error) {
            console.log(error);
        }
    }

    async clear(){
        await AsyncStorage.clear();
    }

};

export default Events;