
import React from 'react'
import Thing from './Thing';
import GLOBALS from './Global';
import { Client, Message } from 'react-native-paho-mqtt';
import Events from './events';
import {StyleSheet,Text,View,TextInput,FlatList,TouchableOpacity } from 'react-native';




/*const address=[
    {'ip':'192.168.0.1','mac': '00:11:22:33:44:55','name':'left window'},
    {'ip':'192.168.0.1','mac': '00:11:22:33:44:55','name':'right window'},
    {'ip':'192.168.0.1','mac': '00:11:22:33:44:55','name':'back window'}
];
*/

class Home extends React.Component {   
    state={
        address:[
            {'ip':'192.168.0.1','mac': '00:11:22:33:44:55','name':'left window'}
        ],
        infos:[] 
    }


    async componentDidMount() {
        this.setState({value: this.props.value});

        const events = new Events();
        this.state.empty = await events.getEventLogs().lenght == 0 ? true : false;

        this.client = new Client({ uri: 'ws://test.mosquitto.org:8081/', clientId: 'test' });
        this.client.connect()
        .then(() => {
            console.log('onConnect');
            return this.client.subscribe(`/IOT3/STATES`); 
            })
                .then(() => {
                console.log('onSubcribe');
            })
                    .then(() => {
                    const message = new Message(JSON.stringify({id: "", cmd: GLOBALS.MQTT_CDM_QUERY_OBJECTS}));
                    //console.log({id: "", cmd: GLOBALS.MQTT_CDM_QUERY_OBJECTS})
                    message.destinationName = `/IOT3/COMMANDS`;
                    this.client.send(message);
            });

            this.client.on('messageReceived', async (message) => {
               console.log(message.payloadString);
                const data = JSON.parse(message.payloadString);
                console.log(data.ip);
                console.log(data.mac);
                console.log(data.name);
                this.state.address.push({'ip':`${data.ip}`,'mac':`${data.mac}`,'name':`${data.name}`})
                console.log(this.state.address)
            })
    }   
    
    getInfo(name){
      const resultat = this.state.address.find( info => info.name === name);
      this.state.infos.push(resultat['ip']);
      this.state.infos.push(resultat['mac']);
      this.state.infos.push(resultat['name']);
      //console.log(infos);
  }
  
  constructor(props){
    super(props); 
    
    this.Array_Items = this.state.address
}
  render()
  {


    return(
        <View style={styles.container}>

            <View style={styles.formContent}>
                <View style={styles.inputContainer}>        
                <TextInput style={styles.inputs}
                    ref={'txtPassword'}
                    placeholder="Recherche d'un store..."
                    underlineColorAndroid='solid'
                    onChangeText={(name_address) => this.setState({name_address})}/>
                </View>

                </View>
                    <FlatList
                        keyExtractor={(item) => item.name}
                        data={this.state.address}
                        renderItem={({item}) => (
                     <View style={styles.notificationBox}>
                         
                    <TouchableOpacity
                        onPressIn={() => this.getInfo(item.name)}
                        onPressOut={() => this.props.navigation.navigate('Thing',{bd:this.state.infos})}
                        >           
                        <Text style={styles.item}>{item.name}</Text>
                    </TouchableOpacity>
                     </View>
                    )}
                    
                    />
                <View style={styles.footer}>
                    <TouchableOpacity
                         onPress={() => this.props.navigation.navigate('Ajout')}>
                         <Text style={styles.name}>+ Ajout un store</Text>
                     </TouchableOpacity>    
                </View>
        </View>        
        
    )}}

const styles = StyleSheet.create(
{
  container:
  {
    flex: 1,
    backgroundColor : "#232426",
    paddingTop: 40,
    paddinfHor: 20
  },
  inputs:{
    height:45,
    marginLeft:16,
    borderBottomColor: '#FFFFFF',
    flex:1,
},
notificationBox:{
    
},
  item:{
      marginTop: 24,
      padding: 15,
      backgroundColor: "#04BF68",
      fontSize: 15,
      marginHorizontal: 10,
      marginTop: 24,
      borderRadius: 20,
      textAlign: 'center',
      fontWeight: 'bold',

  },
    formContent:{
        flexDirection: 'row',
        marginTop:30,    
  },
    inputContainer: {
        borderBottomColor: '#E6E6E6',
        backgroundColor: '#E6E6E6',
        borderRadius:30,
        borderBottomWidth: 1,
        height:45,
        flexDirection: 'row',
        alignItems:'center',
        flex:1,
        margin:50,
    },
    footer:{
        backgroundColor: '#04BF68',
        justifyContent: 'center', 
        alignItems: 'center', 
        flex:0.3,
        height: 60
    },
    name:{
        fontSize:20,
        fontWeight: 'bold',
        color: "#000000",
        marginLeft:10,
        alignSelf: 'center'
    },

});
export default Home





