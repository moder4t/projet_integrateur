import React from 'react';
import GLOBALS from './Global';
import { StyleSheet, View, Text, Switch, TouchableOpacity, Button} from 'react-native';
import { Client, Message } from 'react-native-paho-mqtt';
import Events from './events';

class Thing extends React.Component {

    async componentDidMount() {
        this.setState({value: this.props.value});

        const events = new Events();
        this.state.empty = await events.getEventLogs().lenght == 0 ? true : false;

        this.client = new Client({ uri: 'ws://test.mosquitto.org:8081/', clientId: 'test' });
        this.client.connect()
        .then(() => {
            console.log('onConnect');
            return this.client.subscribe(`/IOT3/STATES`); 
            })
                .then(() => {
                console.log('onSubcribe');
            });


            this.client.on('messageReceived', async (message) => {
               console.log(message.payloadString);
                /* switch{
                     'OPEN_BLINDS': OUVERT,
                     'CLOSE_BLINDS': FERMER
                 }*/
            })
    }


    setBlindToManualOpen(ip,mac){
        // 
        const message = new Message(JSON.stringify({ id: "", cmd: GLOBALS.MQTT_CMD_OPEN_BLINDS }));
        message.destinationName = `/IOT3/COMMANDS/${ip}/${mac}`;
        this.client.send(message);
    }

    setBlindToManualClose(ip,mac) {    
        // 
        const message = new Message(JSON.stringify({ id: "", cmd: GLOBALS.MQTT_CMD_CLOSE_BLINDS }));
        message.destinationName = `/IOT3/COMMANDS/${ip}/${mac}`;
        this.client.send(message);
    }

    askState(ip,mac){
        const message = new Message(JSON.stringify({id: "", cmd: GLOBALS.MQTT_TOPIC_STATES}));
        //console.log({id: "", cmd: GLOBALS.MQTT_CDM_QUERY_OBJECTS})
        message.destinationName = `/IOT3/COMMANDS/${ip}/${mac}`;
        this.client.send(message);
    }

    render() {
        const { navigation } = this.props;
        const BD = navigation.getParam('bd');
        console.log(BD)
        this.askState(BD[0],BD[1])
        return (
            
            <View style={styles.container}>

                <View style={styles.infos}>
                    <Text style={styles.name}>{this.props.caption}</Text>
                    <Text style={styles.value}>Nom du store:  {BD[2]}</Text>                    
                    <Text style={styles.value}>Address IP:  {BD[0]}</Text>
                    <Text style={styles.value}>Mac Address:  {BD[1]}</Text>                    
                    <Text style={styles.value}>Etat du store: presentement {this.props.value ? 'OUVERT': 'FERMER'}</Text>
                </View>
                
                <View style={styles.manuel}>
                    <Text style={styles.innerText}>Mode manuel</Text>
                    <Switch value={this.props.value} 
                        onValueChange={(value)=>{
                            this.props.onValueChange(this.props.caption, value);
                        }}>
                    </Switch>
                    <Button style={styles.buttonOnOff}
                        title="Ouvrir"
                        onPress={() => this.setBlindToManualOpen(BD[0],BD[1])}
                    />
                    <Button style={styles.buttonOnOff}
                        title="Fermer"
                        onPress={() => this.setBlindToManualClose(BD[0],BD[1])}
                    />
                </View>
                <View style={styles.delete}>
                <TouchableOpacity
                    style={styles.bottomView}
                    onPress={() => this.props.navigation.deleteItem()}
                    >                
                <Text style={styles.footer}>Suprimer ce store</Text>
                </TouchableOpacity>
                </View>
            </View>
        )
    }
}      

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#232426',
        },

    infos: {
        alignItems: 'center',
        justifyContent: 'center',
        flex: 1,
    },

    switch: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
        marginTop: 5,
        marginLeft: 5,
        marginRight: 5,
        marginBottom: 5,
        padding:10,
    },

    buttonOnOff:{
        width: '25%', 
        height: 50, 
        backgroundColor: '#04BF68', 
        justifyContent: 'center', 
        alignItems: 'center',
        flexDirection:'row',
        borderRadius:10,
        marginBottom:5,
    },

    manuel:{
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
        marginTop: 5,
    },

    bottomView:{
 
        width: '100%', 
        height: 50, 
        backgroundColor: '#04BF68', 
        justifyContent: 'center', 
        alignItems: 'center',
        flexDirection:'row',
        borderRadius:10,
        marginBottom:5,
      },  

    innerText:{
        color: "white",
    },
    value:{
        color: "white",
    },
    footer:{
        fontWeight: 'bold'
    }
});
export default Thing




