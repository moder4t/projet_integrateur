import React, { Component } from 'react';
import {
  StyleSheet,
  Text,
  View,
  TextInput,
  TouchableHighlight,
  Image,
  Alert
} from 'react-native';
import Events from './events';

export default class Ajout extends Component {
  
  async componentDidMount() {
    this.setState({value: this.props.value});
    // Check if the event log is empty
    const events = new Events();
    this.state.empty = await events.getEventLogs().lenght == 0 ? true : false;

    // Perform MQTT initialisation
    this.client = new Client({ uri: 'ws://test.mosquitto.org:8080/', clientId: 'test' });
    this.client.connect()
    .then(() => {
        console.log('onConnect');
        return this.client.subscribe(`/IOT3/STATES`); 
        })
            .then(() => {
            console.log('onSubcribe');
        });

        // IoT state has changed {"id": "SmartPlugStub", "cmd": "off"}
        this.client.on('messageReceived', async (message) => {
            console.log(message.payloadString);
        })
}
getQueryObjects() {
  const message = new Message(JSON.stringify({ id: "", cmd: GLOBALS.MQTT_CDM_QUERY_OBJECTS}));
  message.destinationName = `/IOT3/COMMANDS/`;
  this.client.send(message);
}
getQueryObjects(){}

    
      render() {
        return (
          <View style={styles.container}>
           <Image style={styles.logo} source={{uri: 'https://png.icons8.com/google/color/120'}}/>
    
            <View style={styles.inputContainer}>
              <Image style={styles.inputIcon} source={{uri: 'https://png.icons8.com/user/ultraviolet/50/3498db'}}/>
              <TextInput style={styles.inputs}
                  placeholder="Mac adress"
                  underlineColorAndroid='transparent'
                  onChangeText={(email) => this.setState({email})}/>
            </View>
            
            <View style={styles.inputContainer}>
              <Image style={styles.inputIcon} source={{uri: 'https://png.icons8.com/message/ultraviolet/50/3498db'}}/>
              <TextInput style={styles.inputs}
                  placeholder="IP Adress"
                  underlineColorAndroid='transparent'
                  onChangeText={(password) => this.setState({password})}/>
            </View>
    
            
            <View style={styles.inputContainer}>
              <Image style={styles.inputIcon} source={{uri: 'https://png.icons8.com/speech-bubble/ultraviolet/50'}}/>
              <TextInput style={styles.inputs}
                  placeholder="Nom du store"
                  underlineColorAndroid='transparent'
                  onChangeText={(password) => this.setState({password})}/>
            </View>
    
            <TouchableHighlight style={[styles.buttonContainer, styles.sendButton]} onPress={() => this.add('login')}>
              <Text style={styles.buttonText}>Ajouter</Text>
            </TouchableHighlight>
          </View>
        );
      }
    }
    


const styles = StyleSheet.create({
    container: {
      flex: 1,
      justifyContent: 'center',
      alignItems: 'center',
      backgroundColor: '#232426',
    },
    logo:{
      width:120,
      height:120,
      justifyContent: 'center',
      marginBottom:20,
    },
    inputContainer: {
        borderBottomColor: '#F5FCFF',
        backgroundColor: '#FFFFFF',
        borderRadius:30,
        borderBottomWidth: 1,
        width:250,
        height:45,
        marginBottom:20,
        flexDirection: 'row',
        alignItems:'center'
    },
    inputs:{
        height:45,
        marginLeft:16,
        borderBottomColor: '#FFFFFF',
        flex:1,
    },
    inputIcon:{
      width:30,
      height:30,
      marginLeft:15,
      justifyContent: 'center'
    },
    buttonContainer: {
      height:45,
      flexDirection: 'row',
      justifyContent: 'center',
      alignItems: 'center',
      marginBottom:20,
      width:100,
      borderRadius:30,
    },
    sendButton: {
      backgroundColor: "#04BF68",
    },
    buttonText: {
      color: 'white',
    }
  });
