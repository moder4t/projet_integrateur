
#include <IoT3.h>
#include <ESP8266WiFi.h>
#include <PubSubClient.h>
#include <ArduinoJson.h> 

/** Program constants */
#define MQTT_SERVER              "10.0.0.247"
#define MQTT_CLIENT              "julyd"
#define MQTT_TOPIC_COMMANDS      "/IOT3/COMMANDS"
#define MQTT_TOPIC_STATES        "/IOT3/STATES"
#define MQTT_CMD_OPEN_BLINDS     "OPEN_BLINDS"
#define MQTT_CMD_CLOSE_BLINDS    "CLOSE_BLINDS"
#define MQTT_STATE_DEVICE_READY  "READY"
#define MQTT_STATE_BLINDS_OPENED "BLINDS_OPEN"
#define MQTT_STATE_BLINDS_CLOSED "BLINDS_CLOSE"
#define MQTT_MAX_PAYLOAD         256

/** Program variables */
WiFiClient wifiClient;
PubSubClient client(wifiClient);
extern Blinds blinds;

void BlindsStub::callback(char* topic, byte* payload, unsigned int length) 
{
    /** Unpack the payload */
    StaticJsonDocument<MQTT_MAX_PAYLOAD> doc;
    deserializeJson(doc, payload, length);
    const char* cmd = doc["cmd"];

    /** Dispatch the command */
    if (String(MQTT_CMD_OPEN_BLINDS) == cmd) {
        blinds.open();
    }
    else if (String(MQTT_CMD_CLOSE_BLINDS) == cmd) {
        blinds.close();
    }
}

BlindsStub::BlindsStub() {
}

void BlindsStub::setup() {

    /** Load WiFi credentials */
    Repos repos;
    repos.load();

    /** Init. WiFi client */
    WiFi.mode(WIFI_STA);
    WiFi.begin(repos.getSSID(), repos.getPassword());
    // WiFi.begin("Grimard Wifi", "*******");
    while (WiFi.status() != WL_CONNECTED) {
      delay(500);
    }

    /** Init. MQTT client */
    pinMode(LED_BUILTIN, OUTPUT);

    client.setServer(MQTT_SERVER, 1883);
    client.setCallback(callback);
    while (!client.connected()) {
        if (!client.connect(MQTT_CLIENT)) {
            delay(5000);
        }
    }
    client.subscribe(MQTT_TOPIC_COMMANDS);
    client.publish(MQTT_TOPIC_STATES, MQTT_STATE_DEVICE_READY);

    /** Init. the blinds */
    blinds.setup();
}

void BlindsStub::loop() {
    client.loop();
    blinds.loop();
}

void BlindsStub::onOpen() {
    char* msg = MQTT_STATE_BLINDS_OPENED;
    client.publish(MQTT_TOPIC_STATES, msg);
}

void BlindsStub::onClose() {
    char* msg = MQTT_STATE_BLINDS_CLOSED;
    client.publish(MQTT_TOPIC_STATES, msg);
}
