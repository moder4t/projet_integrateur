
#include <IoT3.h>
#include <EEPROM.h>

int Repos::offset(void* field) {
    return (byte*)field - (byte*)this;
}

Repos::Repos() {
}

bool Repos::load() {
    EEPROM.begin(sizeof(*this));
    EEPROM.get(offset(ssid), ssid);
    EEPROM.get(offset(password), password);
    EEPROM.get(offset(&ok), ok);
    return ok;
}

String Repos::getSSID() {
    return String(ssid);
}

String Repos::getPassword() {
    return String(password);
}

void Repos::setSSID(const String& str) {
    str.toCharArray(ssid, str.length() + 1);
}

void Repos::setPassword(const String& str) {
    str.toCharArray(password, str.length() + 1);
}

void Repos::save() {
    EEPROM.begin(sizeof(*this));
    EEPROM.put(offset(ssid), ssid);
    EEPROM.put(offset(password), password);
    EEPROM.put(offset(&ok), true);
    EEPROM.commit();
    EEPROM.end();
}

String Repos::toString() {
    String string = 
                "{\n";
    string += "\t'SSID': '" + getSSID() + "',\n";
    string += "\t'Password': '" + getPassword() + "'\n";
    string += "}\n\n";
    return string;
}
