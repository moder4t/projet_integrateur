
#include <IoT3.h>

extern BlindsStub blindsStub;

Blinds::Blinds(const BlindsObserver& observer) : observer(observer) {
}

void Blinds::open() {
    digitalWrite(LED_BUILTIN, LOW);
    blindsStub.onOpen();
}

void Blinds::close() {
    digitalWrite(LED_BUILTIN, HIGH);
    blindsStub.onClose();
}

void Blinds::setup() {
    pinMode(LED_BUILTIN, OUTPUT);
    digitalWrite(LED_BUILTIN, LOW);
}

void Blinds::loop() {
}
