# IoT Project #3
set PATH=%PATH%;C:\Program Files\mosquitto

### 1) Mosquitto MQTT message broker installation:

```bash
sudo apt-add-repository ppa:mosquitto-dev/mosquitto-ppa
sudo apt-get update
sudo snap install mosquitto
sudo apt install mosquitto-clients
sudo apt-get update
```

### 2) To subscribe to an MQTT topic, type the following command:

```bash
mosquitto_sub -t "/IOT3/STATES"
```

### 3) To publish a message, type the following command:

```bash
mosquitto_pub -t "/IOT3/COMMANDS" -m '{"cmd": "CLOSE_BLINDS"}'
mosquitto_pub -t "/IOT3/COMMANDS" -m '{"cmd": "OPEN_BLINDS"}'
```

### 4) To detrmine your IP address, type the following command:

```bash
ip addr show
```

"{\"sensor\":\"gps\",\"time\":1351824120,\"data\":[48.756080,2.302038]}"

### 5) To install mosquitto 

