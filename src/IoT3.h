
#ifndef IOT3_H
#define IOT3_H

#include <Arduino.h>

class Repos;
class BlindsObserver;
class Firmware;
class SoftAccessPoint;
class BlindsStub;
class Blinds;

#define MAX_SSID      32
#define MAX_PASSWORD  63

class Repos {
  char ssid[MAX_SSID];
  char password[MAX_PASSWORD];
  bool ok;
public:
    int offset(void* field);
    Repos();
    bool load();
    String getSSID();
    String getPassword();
    void setSSID(const String& str);
    void setPassword(const String& str);
    void save();
    String toString();
};

class BlindsObserver {
public:
    virtual void onOpen() = 0;
    virtual void onClose() = 0;
};

class Firmware {
public:
    virtual void setup() = 0;
    virtual void loop() = 0;
};

class SoftAccessPoint : public Firmware {
    static void handleRoot();
    static void handleForm();
    static void handleNotFound();
public:
    SoftAccessPoint();
    virtual void setup();
    virtual void loop();
};

class BlindsStub : 
    public Firmware, 
    public BlindsObserver {
    static void callback(char* topic, byte* payload, unsigned int length);
public:
    BlindsStub();
    virtual void setup();
    virtual void loop();
    virtual void onOpen();
    virtual void onClose();
};

class Blinds : public Firmware {
    const BlindsObserver& observer;
public:
    Blinds(const BlindsObserver& observer);
    void open();
    void close();
    virtual void setup();
    virtual void loop();
};

#endif