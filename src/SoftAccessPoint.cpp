
/** http://192.168.4.1/ */

#include <IoT3.h>
#include <ESP8266WebServer.h>

/** WiFi access point default credancials */
#define APSSID "THEMATRIX"
#define APPSK  "thereisnospoon"

/** Program variables */
ESP8266WebServer server(80);

void SoftAccessPoint::handleRoot() {
    const String form = "<html>\
    <head>\
        <title>Blinds WiFi credentials input form</title>\
        <style>\
        body { background-color: #cccccc; font-family: Arial, Helvetica, Sans-Serif; Color: #000088; }\
        </style>\
    </head>\
    <body>\
        <h1>WiFi credentials</h1><br>\
        <form method='post' enctype='application/x-www-form-urlencoded' action='/postform/'>\
        <label>SSID:\
            <input type='text' name='ssid' value='VIDEOTRON6590'><br>\
        </label>Password:\
        <label>\
            <input type='text' name='password'value='A9UAYAHH9947N'><br>\
        </label>\
        <input type='submit' value='Submit'>\
        </form>\
    </body>\
    </html>";
    server.send(200, "text/html", form);
}

void SoftAccessPoint::handleForm() {
    if (server.method() != HTTP_POST) {

        /** Only HTTP_POST is allowed */
        server.send(405, "text/plain", "Method Not Allowed");

    } else {

        /** Save WiFi credentials to EEPROM */
        Repos repos;
        repos.setSSID(server.arg("ssid"));
        repos.setPassword(server.arg("password"));
        repos.save();

        /** Send success message to user */
        String resp = "WiFi credancials successfully saved. Please, reset to continue\n";
        resp += repos.toString();
        server.send(200, "text/plain", resp);
    }
}

void SoftAccessPoint::handleNotFound() {
    String resp = "File Not Found\n\n";
    resp += "URI: ";
    resp += server.uri();
    resp += "\nMethod: ";
    resp += (server.method() == HTTP_GET) ? "GET" : "POST";
    resp += "\nArguments: ";
    resp += server.args();
    resp += "\n";
    for (uint8_t i = 0; i < server.args(); i++) {
        resp += " " + server.argName(i) + ": " + server.arg(i) + "\n";
    }
    server.send(404, "text/plain", resp);
}

SoftAccessPoint::SoftAccessPoint() {
}

void SoftAccessPoint::setup() {
    
    /** perform WiFi access point initialisation */
    WiFi.softAP(APSSID, APPSK);
    WiFi.softAPIP();
    server.on("/", handleRoot);
    server.on("/postform/", handleForm);
    server.onNotFound(handleNotFound);
    server.begin();
}

void SoftAccessPoint::loop() {
    server.handleClient();
}
