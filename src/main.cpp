
#include <IoT3.h>

SoftAccessPoint softAccessPoint;
BlindsStub blindsStub;
Blinds blinds(blindsStub);
Firmware* firmware = NULL;

void setup() {
    Repos repos;
    firmware = repos.load() ? (Firmware*)&blindsStub : (Firmware*)&softAccessPoint;
    firmware->setup();
}

void loop() {
    firmware->loop();
}